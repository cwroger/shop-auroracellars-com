'use strict';

var gulp           = require('gulp'),
    sass           = require('gulp-sass'),
    postcss        = require('gulp-postcss'),
    connect        = require('gulp-connect'),
    uglify         = require('gulp-uglify'),
    fs             = require('fs');

var paths = {
    base_build:     'build',
    base_staging:   'staging',

    assets_css:     '/assets/css',
    assets_js:      '/assets/js',
    assets_images:  '/assets/images',

    src_css:        'src/styles/**/*.scss',
    src_js:         'src/scripts/**/*.js',
    src_images:     'src/images/**/*',
};


/**
 * Load package data the Right Way(tm).
 * It never changes and for speed we'd rather just load it once.
 */
var package_data = JSON.parse(fs.readFileSync('./package.json'));


function runConnect() {
    connect.server({
        root: paths.base_staging,
        livereload: {
          port: 35728,
        },
        port: 8081
    });
}

var html = (function () {
    let plugins = {
        nunjucks   : require('gulp-nunjucks-render'),
        prettyHtml : require('gulp-pretty-html'),
    };

    let settings = {
        nunjucks: {
            path: [
                'src/pages/inc',
                'src/pages'
            ],
            inheritExtension: true,
            envOptions: {
                trimBlocks: true,
                lstripBlocks: true
            },
            data: {
                site_url: package_data.mainSite
            }
        },
        prettyHtml: {
            indent_with_tabs: true,
            extra_liners: [],
            max_preserve_newlines: 0
        }
    };
    return {
        renderStaging: function() {
            return gulp.src('src/pages/*.+(html|htm)')
                .pipe(plugins.nunjucks(settings.nunjucks))
                .pipe(plugins.prettyHtml(settings.prettyHtml))
                .pipe(gulp.dest(paths.base_staging))
                .pipe(connect.reload());
        },
        renderProduction: function() {
            var nunjucksSettings = settings.nunjucks;
            nunjucksSettings.data.production = true;

            return gulp.src('src/pages/*.htm')
                .pipe(plugins.nunjucks(nunjucksSettings))
                .pipe(plugins.prettyHtml(settings.prettyHtml))
                .pipe(gulp.dest(paths.base_build));
        }
    };
}());
exports.buildHtml = gulp.series( html.renderStaging, html.renderProduction );


function buildStyles() {
    var processors = [
        require('pixrem')(),
        require('cssnano')({
            preset: ['default', {
                normalizeWhitespace: false,
                zindex: false,
            }],
        }),
        require('autoprefixer')(),
    ];

    return gulp.src(paths.src_css)
        .pipe(sass({
            outputStyle: 'expanded',
            includePaths: [
                require('scss-resets').includePaths,
                'production/assets/css',
            ],
        }).on('error', sass.logError))
        .pipe(postcss(processors))
        .pipe(gulp.dest(paths.base_staging + paths.assets_css))
        .pipe(connect.reload());
}
exports.buildStyles = buildStyles;


function buildScripts() {
    return gulp.src(paths.src_js)
        .pipe(uglify({
            output: {
                beautify: true,
                comments: 'license',
            },
            compress: false,
            mangle: false
        }))
        .pipe(gulp.dest(paths.base_staging + paths.assets_js))
        .pipe(connect.reload());
}
exports.buildScripts = buildScripts;


var copy = {
    images: function() {
        return gulp.src(paths.src_images)
            .pipe(gulp.dest(paths.base_staging + paths.assets_images))
            .pipe(gulp.dest(paths.base_build + paths.assets_images));
    },
    vendorjs: function() {
        return gulp.src([
            "./node_modules/Respond.js/dest/respond*.min.js",
            "./node_modules/html5shiv/dist/html5shiv-printshiv.min.js",
        ])
            .pipe(gulp.dest(paths.base_staging + paths.assets_js))
            .pipe(gulp.dest(paths.base_build + paths.assets_js));
    }
};

function watch() {
    gulp.watch(paths.src_css, buildStyles);
    gulp.watch(paths.src_js, buildScripts);
    gulp.watch('src/pages/**/*.+(njx|html|htm)', html.renderStaging);
}

exports.watch = gulp.parallel(
    runConnect,
    watch
);
exports.build = gulp.series(
    copy.images,
    copy.vendorjs,
    buildStyles,
    buildScripts,
    html.renderStaging,
    html.renderProduction
);
